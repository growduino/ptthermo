/*****************************************************

Sketch to measure temperature from pt1000 thermometer
apply calibration 
and display both values on 16x2 lcd.

BSD license 

*****************************************************/
#include <Wire.h>
#include <LiquidCrystal_I2C.h>
LiquidCrystal_I2C lcd(0x27,16,2); 

#include <Adafruit_MAX31865.h>

// Use software SPI: CS, DI, DO, CLK
Adafruit_MAX31865 max = Adafruit_MAX31865(10, 11, 12, 13);

#define RREF      4300.0
#define RNOMINAL  1000.0

/*****************************************************
Calibration values

_pt values are what is displayed as "raw" on arduino display,

_outside values are from trusted thermometer

*****************************************************/
float temp1_pt = 0;
float temp1_outside = 0;

float temp2_pt = 100;
float temp2_outside = 100;

/*****************************************************/

float slope = (temp2_outside - temp1_outside) / (temp2_pt - temp1_pt);
float offset = temp1_outside - temp1_pt * slope;

void setup() {
  Serial.begin(115200);
  Serial.println("Adafruit MAX31865 PT100 Sensor Test!");
  lcd.init();
  // Print a message to the LCD.
  lcd.print("hello, world!");

  max.begin(MAX31865_3WIRE);  // set to 2WIRE or 4WIRE as necessary
  lcd.setBacklight(HIGH);
}

void lcd_print(String msg) {
  Serial.println(msg);
  lcd.clear();
  lcd.setCursor(0,0);
  lcd.print(msg);
}

void lcd_print_low(String msg) {
  Serial.println(msg);
  lcd.setCursor(0,1);
  lcd.print(msg);
}

void loop() {
  uint16_t rtd = max.readRTD();


  float temp = max.temperature(RNOMINAL, RREF);
  String line1 = String("Raw t: ") + String(temp);
  String line2 = String("Adj t: ") + String( temp * slope + offset );
  lcd_print(line1);
  lcd_print_low(line2);



  // Check and print any faults
  uint8_t fault = max.readFault();
  if (fault) {
    Serial.print("Fault 0x"); Serial.println(fault, HEX);
    if (fault & MAX31865_FAULT_HIGHTHRESH) {
      Serial.println("RTD High Threshold"); 
    }
    if (fault & MAX31865_FAULT_LOWTHRESH) {
      lcd_print("RTD Low Threshold"); 
    }
    if (fault & MAX31865_FAULT_REFINLOW) {
      lcd_print("REFIN- > 0.85 x Bias"); 
    }
    if (fault & MAX31865_FAULT_REFINHIGH) {
      lcd_print("REFIN- < 0.85 x Bias - FORCE- open"); 
    }
    if (fault & MAX31865_FAULT_RTDINLOW) {
      lcd_print("RTDIN- < 0.85 x Bias - FORCE- open"); 
    }
    if (fault & MAX31865_FAULT_OVUV) {
      lcd_print("Under/Over voltage"); 
    }
    max.clearFault();
  } else {
    // pass
  }
  Serial.println();
  delay(1000);
}
